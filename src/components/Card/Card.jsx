import './Card.css';
import { useDispatch, useSelector } from 'react-redux';
import { setModify } from '../../state/modifySlice/modifySlice';
import { useNavigate } from 'react-router-dom';
import { getCorsi, createLog, getUser, editUser, getUsers } from '../../services/firebase-service';
import { setCorsi } from '../../state/corsi/corsiSlice';
import { setUser } from '../../state/user/userSlice';
import { image } from '../../services/utilities';
import { deletion } from '../../services/utilities';
import swal from 'sweetalert2';

export function Card({ corso }) {

    const dispatch = useDispatch();

    const user = useSelector(state => state.user.value);

    const navigateTo = useNavigate();

    const corsoImage = image.find(element => element.categoria === corso.categoria)?.image;

    function handleModify(id) {
        dispatch(setModify({
            idCorso: id,
        }));
        navigateTo("/modifica");
    }

    async function listaCorsi() {
        const data = await getCorsi();
        dispatch(setCorsi({
            corsiCorrenti: [...data]
        }))
    }

    async function handleDelete(id) {
        await deletion(id);
        const users = await getUsers();
        for (let element of users) {
            const index = element.corsiSeguiti.indexOf(id);
            if (index !== -1) {
                const updatedCorsi = element.corsiSeguiti.filter(courseId => courseId !== id);
                const updatedUser = {
                    ...element,
                    corsiSeguiti: updatedCorsi
                };
                await editUser(element.id, updatedUser);
            }
        }
        await createLog(`INFO: Corso di ID: ${id} è stato cancellato.`)
        swal.fire({
            title: 'Corso eliminato correttamente',
            icon: 'info',
            confirmButtonText: 'Continua',
            confirmButtonColor: 'red'
        });
        listaCorsi();
    }

    async function handleSubscription(id) {
        if (!user.corsiSeguiti.includes(id)) {
            const modifiedUser = {
                nome: user.nome,
                cognome: user.cognome,
                email : user.email,
                role: [...user.ruolo],
                corsiSeguiti: [...user.corsiSeguiti, id],
                isLogged: true
            }
            const currentUser = await getUser(user.email);
            await editUser(currentUser.id, modifiedUser);
            const updatedUser = await getUser(user.email);
            dispatch(setUser({
                nome: updatedUser.nome,
                cognome: updatedUser.cognome,
                email : updatedUser.email,
                ruolo: [...updatedUser.role],
                corsiSeguiti: [...updatedUser.corsiSeguiti],
                isLogged: true
            }))
            swal.fire({
                title: 'Ti sei iscritto correttamente!',
                icon: 'success',
                confirmButtonText: 'Continua a cercare',
                confirmButtonColor: 'green'
            })
            await createLog(`INFO: Iscrizione dell'utente ${user.email} al corso di ID: ${id}`)
        }
        else {
            swal.fire({
                title: 'Ti sei iscritto a questo corso',
                icon: 'error',
                confirmButtonText: 'Riprova',
                confirmButtonColor: 'red'
            });
            await createLog(`ERROR: Tentativo di iscrizione dell'utente ${user.email} al corso di ID: ${id}`)
        }
    }

    return (
        <div className="card">
            <img className="card-img-top" src={corsoImage} alt={corso.categoria} />
            <div className="card-body">
                <h5 className="card-title">{corso.titolo}</h5>
                <p className='card-sub'>{`Durata di ${corso.durata} ore`}</p>
                <p className="card-text">{corso.descrizione}</p>
                {user.ruolo.includes("Admin") ? 
                <>
                    <button id="modifica" onClick={() => handleModify(corso.id)} className="btn btn-success">Modifica</button>
                    <span className="button-space"></span> 
                    <button id="cancella" onClick={() => handleDelete(corso.id)} className="btn btn-danger">Elimina</button>
                </>
                :
                    <button id="modifica" onClick={() => handleSubscription(corso.id)} className="btn btn-success">Iscriviti</button>
                }
            </div>
        </div>
    );
}
import { useSelector } from "react-redux";
import { Navigate} from "react-router-dom";

export function ProtectedModify({children}) {

    const user = useSelector(state => state.user.value);

    const modify = useSelector(state => state.modify.value);

    if (user.isLogged && user.ruolo.includes("Admin") && modify.idCorso != -1) {
        return <>
        {children}
        </>
    }
    else {
        return <Navigate to={"/"} />
    }
}

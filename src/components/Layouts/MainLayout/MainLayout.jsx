import { useOutlet } from "react-router-dom";
import { Navbar } from "../../Navbar/Navbar";
import { useEffect } from "react";
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";
import { getUser } from "../../../services/firebase-service";
import { setUser } from "../../../state/user/userSlice";
import { useDispatch } from "react-redux";
import { Footer } from "../../Footer/Footer";


export function MainLayout() {

    const dispatch = useDispatch();

    useEffect(() => {
        async function fetchUser() {
            const token = Cookies.get('token');
            if (token) {
                const email = jwtDecode(token).email;
                const user = await getUser(email);   
                if (user) {
                    dispatch(setUser({
                        nome: user.nome,
                        cognome: user.cognome,
                        email: user.email,
                        ruolo: [...user.role],
                        corsiSeguiti: [...user.corsiSeguiti],
                        isLogged: true
                    }))
                }
            }
        }
        fetchUser();
    })
    const outlet = useOutlet()
    return <>
        <Navbar></Navbar>
        {outlet}
        <Footer></Footer>
    </>
}
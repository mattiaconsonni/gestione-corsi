import "./Footer.css";
import { Link } from "react-router-dom";
import instagram from '../../assets/ig.jfif';
import linkedin from '../../assets/linkedin.jfif';
import facebook from '../../assets/fb.jfif';

export function Footer() {
    return (
        <nav className="navbar fixed-bottom navbar-expand-lg navbar-light bg-light custom-footer">
            <div className="container-fluid justify-content-between">
                <div className="navbar-brand">
                    Ninja Developers e-Learning<sup>®</sup>
                </div>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-link" to="/terms-of-service">Terms of Service</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/copyright">Copyright</Link>
                        </li>
                    </ul>
                </div>
                <div className="d-flex">
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul className="navbar-nav social-icons mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link className="nav-link" to="https://instagram.com" target="_blank">
                                    <img src={instagram} alt="instagram" className="social-icon" /> Instagram
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="https://linkedin.com" target="_blank">
                                    <img src={linkedin} alt="linkedin" className="social-icon" /> LinkedIn
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="https://facebook.com" target="_blank">
                                    <img src={facebook} alt="facebook" className="social-icon" /> Facebook
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    );
}
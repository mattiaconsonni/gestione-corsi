import './Navbar.css' // File CSS
import { Link, NavLink, useNavigate } from "react-router-dom"; // Libreria per la navigazione tra pagine web
import { logo } from '../../services/utilities'; // Logo del sito
import { useSelector, useDispatch } from "react-redux"; // Libreria per la definizione dello stato globale
import { setUser } from "../../state/user/userSlice"; // Servizio di set dell'utente a livello globale
import Cookies from "js-cookie"; // Libreria per la gestione dei Cookies


// Componente per la definizione di una barra di navigazione  
export function Navbar() {

    // Hook per la definizione del dispatcher dei reducers
    const dispatch = useDispatch();

    // Recupero dello stato utente
    const user = useSelector(state => state.user.value);
    
    // Hook per permettere la navigazione tra pagine
    const navigateTo = useNavigate();

    // Funzione che gestisce il logout dell'utente
    function handleLogOut() {
        dispatch(setUser({
            nome: "",
            cognome: "",
            email: "",
            role: [],
            corsiSeguiti: [],
            isLogged: false
        }))
        Cookies.remove('token');
        navigateTo("/");
    }

    return <>
        <header>
            <nav id="custom" className="navbar fixed-top navbar-expand-lg bg-body-tertiary navbar border-bottom border-body">
                <div className="container-fluid">
                    <NavLink className="navbar-brand" to="/">
                        <img id="cap" src={logo} alt="logo" width="30" height="30" class="d-inline-block align-text-top"></img> Ninja Developers e-Learning
                    </NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        {user.isLogged &&
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/corsi">Corsi erogati</NavLink>
                            </li>
                        </ul>
                        }
                        {user.isLogged && user.ruolo.includes("Admin") &&
                        <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Amministrazione
                                </a>
                                <ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
                                    <li><Link className="dropdown-item" to={"/utenti"}>Utenti</Link></li>
                                    <li><Link className="dropdown-item" to={"/logs"}>Logs</Link></li>
                                    
                                </ul>
                                </li>
                            </ul>
                        </div>
                        }
                        {!user.isLogged ? 
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/login">Login</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/register">Sign in</NavLink>
                            </li>
                        </ul>
                        :
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/profilo">Profilo personale</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" onClick={handleLogOut} aria-current="page" to="/">LogOut </NavLink>
                            </li>
                        </ul>
                        }       
                    </div>
                </div>
            </nav>
        </header>
    </>
}

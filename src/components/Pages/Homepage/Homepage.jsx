import './Homepage.css'
import { useSelector } from 'react-redux'
import { useEffect } from 'react';
import { Carousel } from '../../Carousel/Carousel';
import { Link } from 'react-router-dom';
import { logo } from '../../../services/utilities';

export function Homepage() {
    
    const user = useSelector(state => state.user.value)

    useEffect(() => {
        document.title = "Homepage"
    }, []); 

    return <>
        <div className="homepage-container">
            <div className="welcome-content">
                <h1 className="welcome-title">La destinazione per chi vuole diventare il programmatore più versatile!</h1>
                <p className="welcome-description">
                    Affronta corsi di ogni tipo con i nostri esperti e diventa uno shinobi dell'informazione!
                    <br />
                    Aumenta le tue conoscenze, impara nuove tecnologie e scopri <i>nuovi linguaggi</i> con <b>Ninja Developers e-Learning</b>.
                    <br />
                    In questa app potrai scegliere i corsi che ti interessano e frequentarli quando vuoi e dove vuoi... come un ninja!
                </p>
            </div>
        </div>
        <div className='carousel-container'>
            <Carousel />
        </div>

        <div className="know-content">
            <h1 className="welcome-title">Iscriviti anche tu al DOJO e segui la via della Computer Science</h1>
            <p className="welcome-description">
                Incontra i nostri sensei che ti accoglieranno e ti insegneranno al meglio delle loro possibilità.
                <br />
                Segui la tua via e le tue passione per impostare la tua carriera nel mondo dell'informatica!
                <br />
                {!user.isLogged &&
                <b>Sei interessato a lavorare con noi? </b>
                }
            </p>
            {!user.isLogged &&
            <Link className='btn btn-danger' to={"/register"} id='learn-more'>Clicca qui Sensei!</Link>
            }
        </div>
        <br />
        <div className="about-us">
            <img src={logo} alt="NinjaDev Logo" className="about-us-logo"/>
            <div>
                <p className="about-us-text">
                    Noi di Ninja Developers cerchiamo sempre nuovi insegnanti che permettano a tutti i nostri appassionati
                    ragazzi di crescere nel loro percorso di arti computazionali e un giorno diventare i sensei del loro dojo...
                </p>
                <h1 className='about-us-header'>COSA STATE ASPETTANDO?</h1>
                <p className="about-us-subtext">
                    Seguite la via della tecnologia seguendo il nostro motto:
                    <br /><br />
                    <cite><i>Metti un bug, togli un Bug, metti un bug...</i></cite>
                </p>
            </div>
        </div>
    </>
}
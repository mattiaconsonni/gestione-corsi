// Importazione dei file CSS
import './Registrazione.css';
import '../../../services/CustomSwal.css'
import { useForm } from "react-hook-form"; // Libreria per la gestione dei form
import { useNavigate } from 'react-router-dom'; // Libreria per la gestione della navigazione tra pagine
import { useEffect } from "react"; //Hook per gestire gli effetti collaterali
import { registerUser } from "../../../services/firebase-service";  // Funzione di servizio per registrare l'utente
import swal from 'sweetalert2'; // Libreria per mostrare popup di notifica
import { createLog } from '../../../services/firebase-service'; // Funzione di servizio per la creazione di Logs

// Definizione del componente di registrazione
export function Registrazione() {

    // Uso dell'hook useEffect per impostare il titolo della pagina quando il componente viene montato
    useEffect(() => {
        document.title = "Registrazione";
    }, []);

    // Uso dell'hook useNavigate per permettere la navigazione verso altre pagine
    const navigateTo = useNavigate();

    // Inizializzazione del form con react-hook-form e definizione dei valori predefiniti
    const {
        register,           // Funzione per registrare i campi del form
        handleSubmit,       // Funzione per gestire l'invio del form
        formState: { errors } // Oggetto che contiene gli errori del form
    } = useForm({
        defaultValues: {
            nome: "",
            cognome: "",
            email: "",
            password: "",
            confirmPassword: ""
        }
    });

    // Funzione asincrona da chiamare al submit del form
    const onSubmit = async (data) => {
        // Controllo se le password coincidono
        if (data.password !== data.confirmPassword) {
            // Mostra un messaggio di errore se le password non coincidono
            swal.fire({
                title: "Qualcosa è andato storto...",
                text: "Le password non coincidono",
                icon: "error",
                confirmButtonText: 'Riprovare',
                confirmButtonColor: 'red'
            });
            // Registra un log dell'errore
            await createLog("ERRORE: Password non coincidenti");
        } else {
            try {
                // Tenta di registrare l'utente
                await registerUser({
                    nome: data.nome,
                    cognome: data.cognome,
                    email: data.email,
                    password: data.password,
                });
                // Registra un log della registrazione avvenuta con successo
                await createLog(`INFO: registrazione dell'utente ${data.email} eseguita`);
                // Mostra un messaggio di successo
                swal.fire({
                    title: "Registrazione avvenuta con successo!",
                    text: "Ora puoi passare al login",
                    icon: 'success',
                    confirmButtonText: 'Continua',
                    confirmButtonColor: '#28a745'
                }).then(() => {
                    navigateTo("/login");
                });
            } catch (error) {
                // Registra un log di errore in caso di fallimento della registrazione
                await createLog("Error: errore in registrazione");
                // Mostra un messaggio di errore se l'email è già utilizzata
                swal.fire({
                    title: "Qualcosa è andato storto...",
                    text: "Mail già utilizzata",
                    icon: "error",
                    confirmButtonText: 'Riprovare',
                    confirmButtonColor: 'red'
                });
            }
        }
    };

    // Render del form di registrazione
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <h2 style={{fontFamily: 'monospace'}}>Sign in</h2>
            <br /><br />
            {/* Campo per il nome */}
            <div className="form-group">
                <label htmlFor="nome">Nome</label>
                <input
                    type="text"
                    className="form-control"
                    id="nome"
                    placeholder="Inserire il nome"
                    {...register("nome", {
                        required: 'Questo campo è obbligatorio',
                        minLength: {
                            value: 1,
                            message: "Lunghezza minima: 1"
                        }
                    })}
                />
                <p>{errors.nome?.message}</p>
            </div>
            {/* Campo per il cognome */}
            <div className="form-group">
                <label htmlFor="cognome">Cognome</label>
                <input
                    type="text"
                    className="form-control"
                    id="cognome"
                    placeholder="Inserire il cognome"
                    {...register("cognome", {
                        required: 'Questo campo è obbligatorio',
                        minLength: {
                            value: 1,
                            message: "Lunghezza minima: 1"
                        }
                    })}
                />
                <p>{errors.cognome?.message}</p> 
            </div>
            {/* Campo per l'email */}
            <div className="form-group">
                <label htmlFor="email">E-mail</label>
                <input
                    type="email"
                    className="form-control"
                    id="email"
                    placeholder="Inserire la propria e-mail"
                    {...register("email", {
                        required: 'Questo campo è obbligatorio',
                        minLength: {
                            value: 1,
                            message: "Lunghezza minima: 1"
                        }
                    })}
                />
                <p>{errors.email?.message}</p>
            </div>
            {/* Campo per la password */}
            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                    type="password"
                    className="form-control"
                    id="password"
                    placeholder="Password"
                    {...register("password", {
                        required: 'Questo campo è obbligatorio',
                        pattern: {
                            value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                            message: "La password deve essere di almeno 8 caratteri e contenere una lettera e un numero"
                        }
                    })}
                />
                <p>{errors.password?.message}</p>
            </div>
            {/* Campo per confermare la password */}
            <div className="form-group">
                <label htmlFor="confirmPassword">Conferma la password</label>
                <input
                    type="password"
                    className="form-control"
                    id="confirmPassword"
                    placeholder="Conferma la password"
                    {...register("confirmPassword", {
                        required: 'Questo campo è obbligatorio'
                    })}
                />
                <p>{errors.confirmPassword?.message}</p>
            </div>
            {/* Pulsante per inviare il form */}
            <button type="submit" className="btn btn-success">Registrati</button>
        </form>
    );
}

import React, { useEffect, useState } from "react";
import { getUsers, deleteUser, createLog } from "../../../services/firebase-service";
import './Utenti.css'
import Cookies from "js-cookie";
import { useDispatch } from "react-redux";
import { setUser } from "../../../state/user/userSlice";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert2";


export function Utenti() {

    const [users, setUsers] = useState([]);

    const dispatch = useDispatch();

    const navigateTo = useNavigate();

    useEffect(() => {
        fetchUsers();
    }, []);

    async function fetchUsers() {
        const data = await getUsers()
        if (data) {
            setUsers(data);
        }
    }

    console.log(users);


    function handleDelete(id) {
        async function userDeletion() {
            await deleteUser(id);
            await createLog(`INFO: Utente con ID: ${id} è stato eliminato.`)
            await fetchUsers();
        }
        userDeletion();
        Cookies.remove('token');
        dispatch(setUser({
            nome: "",
            cognome : "",
            email: "",
            ruolo: [],
            corsiSeguiti: [],
            isLogged: false
        }))
        swal.fire({
            title: 'Utente eliminato correttamente',
            icon: 'info',
            confirmButtonText: 'Esci',
            confirmButtonColor: 'red'
        }).then(() => {
            navigateTo("/");
        });
    }


    return <>
            <table className="table table-striped table-hover table-bordered">
                <thead className="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Cognome</th>
                        <th scope="col">Ruoli</th>
                        <th scope="col">Elimina</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user, index) => (
                        <tr key={index}>
                            <th scope="row">{index + 1}</th>
                            <td>{user.id}</td>
                            <td>{user.nome}</td>
                            <td>{user.cognome}</td>
                            <td>{user.role.join(", ")}</td>
                            <td>
                                <button onClick={() => handleDelete(user.id)} className="btn btn-danger btn-sm">Elimina</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    
}
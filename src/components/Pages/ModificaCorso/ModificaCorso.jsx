import './ModificaCorso.css'
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createLog } from '../../../services/firebase-service';
import { setModify } from '../../../state/modifySlice/modifySlice';
import { editingCorso } from '../../../services/utilities';
import swal from 'sweetalert2';

export function ModificaCorso() {

    const dispatch = useDispatch();

    const corso = useSelector(state => state.modify.value);
    
    const id = corso.idCorso;

    const navigateTo = useNavigate();

    useEffect(() => {
        document.title = "Modifica del corso";
    }, []);

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        defaultValues: {
            titolo: "",
            durata: 1,
            categoria: "",
            descrizione: "",
        }
    });

    const onSubmit = async (data) => {
        const modifiedCorso = {
            titolo: data.titolo,
            durata : data.durata,
            categoria: data.categoria,
            descrizione: data.descrizione
        }
        await editingCorso(id, modifiedCorso);
        await createLog(`INFO: Corso di id ${id} è stato modificato.`)
        dispatch(setModify({
            idCorso: -1
        }))
        swal.fire({
            title: 'Modifica effettuata!',
            text: "Hai modificato il corso " + data.titolo + "!",
            icon: 'success',
            confirmButtonText: 'Ritorna',
            confirmButtonColor: '#28a745'
        }).then(() => {
            navigateTo("/corsi");
        });
    };


    console.log(errors);

    return  <>
            <form id="modifyForm" onSubmit={handleSubmit(onSubmit)}>
                <h1>Creazione corso!</h1>
                <br />
                <div className="form-group">
                    <label htmlFor="titolo">Titolo del corso</label>
                    <input
                        type="text"
                        className="form-control"
                        id="titolo"
                        placeholder="Inserire il titolo del corso"
                        {...register("titolo", { required: 'Il titolo va inserito!' })}
                    />
                    <p>{errors.titolo?.message}</p>
                </div>
                <div className="form-group">
                    <label htmlFor="durata">Durata del corso</label>
                    <input
                        type="number"
                        className="form-control"
                        id="durata"
                        min={1}
                        max={100}
                        {...register("durata", { required: 'La durata va inserita!' })}
                    />
                    <p>{errors.durata?.message}</p>
                </div>
                <div className="form-group">
                    <select
                        className="custom-select"
                        id="categoria"
                        {...register("categoria", { required: "Inserire la categoria del corso" })}
                    >
                        <option value="" disabled selected>Categoria del corso</option>
                        <option value="cybersecurity">Cybersecurity</option>
                        <option value="programmazione">Programmazione</option>
                        <option value="embedded">Sistemi Embedded</option>
                        <option value="ia">Intelligenza Artificiale</option>
                        <option value="crittografia">Crittograifa</option>
                    </select>
                    <p>{errors.categoria?.message}</p>
                </div>
                <div className="form-group">
                    <label htmlFor="descrizione">Descrizione</label>
                    <input
                        type="text"
                        className="form-control"
                        id="descrizione"
                        placeholder="Inserire la descrizione"
                        {...register("descrizione", { required: "La descrizione va inserita!", minLength: {
                            value: 1,
                            message: "Lunghezza minima: 1"
                        } })}
                    />
                    <p>{errors.descrizione?.message}</p>
                </div>
                <button type="submit" className="btn btn-success">Modifica il corso</button>
            </form>
            </>
}
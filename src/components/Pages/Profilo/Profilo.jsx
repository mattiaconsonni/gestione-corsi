import { useEffect, useState } from "react";
import './Profilo.css'
import { useDispatch, useSelector } from "react-redux";
import { setCorsi } from "../../../state/corsi/corsiSlice";
import { Card } from "../../Card/Card";
import { getCorsi } from "../../../services/firebase-service";

export function Profilo() {

    const user = useSelector(state => state.user.value)

    const corsi = useSelector(state => state.corsi.value)

    const dispatch = useDispatch();

    const [images, setImages] = useState([]);

    async function listaCorsi() {
        const data = await getCorsi();
        dispatch(setCorsi({
            corsiCorrenti: [...data]
        }))
    }

    useEffect(() => {
        document.title = "Profilo personale"
        async function fetchImages() {
            const response = await fetch("https://jsonplaceholder.typicode.com/photos", {
                mode: 'cors',
                method: 'GET'
            });
            if (response.ok) {
                const data = await response.json();
                setImages(data)
            }      
        }
        fetchImages();
        listaCorsi();
    }, []);


    return <>
        <div id="container">
        <div className="profile-card">
            <div className="profile-header">
                {<img className="profile-img" src={images[0]?.url} alt="Immagine del profilo" />}
                <h2>{`${user.nome} ${user.cognome}`}</h2>
                <p>{`Mail: ${user.email}`}</p>
            </div>
            <div className="profile-body">
                <h3>Ruoli</h3>
                <ul className="role-list">
                    {user.ruolo.map((element, index) => (
                        <li className="role-item" key={index}>
                            {`${element}`}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
        {user.corsiSeguiti.length > 0 ? 
            <div className="flex-container">
                <h1 className="headCard">I corsi a cui sei iscritto</h1>
                {corsi.corsiCorrenti
                .filter(element => user.corsiSeguiti.includes(element.id))
                .map((corso, index) => (
                    <Card corso={corso} index={index}/>
                ))}
            </div>
            :
            <div className="warning-container">
                <h1 className="warning-title">Non sei registrato a nessun corso...</h1>
            </div>
        }
    </div>
    </>
}



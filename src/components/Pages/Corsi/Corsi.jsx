import { useEffect } from "react";
import './Corsi.css';
import { useForm } from "react-hook-form";
// Recupero e modifica dello stato
import { useSelector, useDispatch } from "react-redux";
import { createLog, getCorsi } from "../../../services/firebase-service";
import { setCorsi } from "../../../state/corsi/corsiSlice";
import { Card } from "../../Card/Card";
import { corsoGeneration } from "../../../services/utilities";
import swal from "sweetalert2";

export function Corsi() {

    const user = useSelector(state => state.user.value)

    const dispatch = useDispatch();

    async function listaCorsi() {
        const data = await getCorsi();
        dispatch(setCorsi({
            corsiCorrenti: [...data]
        }))
    }

    useEffect(() => {
        document.title = "Lista dei corsi";
        listaCorsi()
    }, []);

    const corsi = useSelector(state => state.corsi.value);

    const {
        register,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm({
        defaultValues: {
            titolo: "",
            durata: 1,
            categoria: "",
            descrizione: "",
        }
    });

    const onSubmit = async (data) => {
        corsoGeneration(data.titolo, data.durata, data.categoria, data.descrizione);
        await createLog(`INFO: Corso creato con ID: ${data.id} e titolo ${data.titolo}`)
        await listaCorsi();
        swal.fire({
            title: 'Corso creato!',
            text: "Hai creato il corso " + data.titolo + "!", 
            icon: 'success',
            confirmButtonText: 'Prosegui',
            confirmButtonColor: '#28a745'
        })
        reset();
    };

    console.log(errors);

    return (
        <>  <div id="container">
            {user.ruolo.includes("Admin") &&
            <form onSubmit={handleSubmit(onSubmit)}>
                <h1>Creazione corso</h1>
                <br />
                <div className="form-group">
                    <label htmlFor="titolo">Titolo del corso</label>
                    <input
                        type="text"
                        className="form-control"
                        id="titolo"
                        placeholder="Inserire il titolo del corso"
                        {...register("titolo", { required: 'Il titolo va inserito!' })}
                    />
                    <p>{errors.titolo?.message}</p>
                </div>
                <div className="form-group">
                    <label htmlFor="durata">Durata del corso</label>
                    <input
                        type="number"
                        className="form-control"
                        id="durata"
                        min={1}
                        max={100}
                        {...register("durata", { required: 'La durata va inserita!' })}
                    />
                    <p>{errors.durata?.message}</p>
                </div>
                <div className="form-group">
                    <select
                        className="custom-select"
                        id="categoria"
                        {...register("categoria", { required: "Inserire la categoria del corso" })}
                    >
                        <option value="" disabled selected>Categoria del corso</option>
                        <option value="cybersecurity">Cybersecurity</option>
                        <option value="programmazione">Programmazione</option>
                        <option value="embedded">Sistemi Embedded</option>
                        <option value="ia">Intelligenza Artificiale</option>
                        <option value="crittografia">Crittograifa</option>
                    </select>
                    <p>{errors.categoria?.message}</p>
                </div>
                <div className="form-group">
                    <label htmlFor="descrizione">Descrizione</label>
                    <input
                        type="text"
                        className="form-control"
                        id="descrizione"
                        placeholder="Inserire la descrizione"
                        {...register("descrizione", { required: "La descrizione va inserita!", minLength: {
                            value: 1,
                            message: "Lunghezza minima: 1"
                        } })}
                    />
                    <p>{errors.descrizione?.message}</p>
                </div>
                <button type="submit" className="btn btn-success">Crea il corso</button>
            </form>
            }
            <div className="flex-container">
                    <h1 className="headCard">Ecco i corsi disponibili</h1>
                    {corsi.corsiCorrenti.map((corso, index) => (
                            <Card corso={corso} index={index} key={index}/>
                        ))}
            </div>
        </div>
        </>
    );
}
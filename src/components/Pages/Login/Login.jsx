// Importazione dei file CSS
import './Login.css'
import { useForm } from "react-hook-form"; // Libreria per la gestione dei form
import { useEffect } from "react"; //Hook per gestire gli effetti collaterali
import { createLog, loginUser } from "../../../services/firebase-service"; // Funzioni di servizio per il login e la creazione di log
import swal from 'sweetalert2'; // Libreria per mostrare popup di notifica
import { useNavigate } from 'react-router-dom'; // Libreria per la gestione della navigazione tra pagine


// Definizione del componente di login
export function Login() {

    // Uso dell'hook useEffect per impostare il titolo della pagina quando il componente viene montato
    useEffect(() => {
        document.title = "Login"
    }, []);

    // Uso dell'hook useNavigate per permettere la navigazione verso altre pagine
    const navigateTo = useNavigate();

    // Inizializzazione del form con react-hook-form e definizione dei valori predefiniti
    const {
        register,           // Funzione per registrare i campi del form
        handleSubmit,       // Funzione per gestire l'invio del form
        formState: { errors } // Oggetto che contiene gli errori del form
    } = useForm({
        defaultValues: {
            email: "",
            password: ""
        }
    });

    // Funzione asincrona da chiamare al submit del form
    const onSubmit = async (data) => {
        try {
            // Tenta di loggare l'utente
            await loginUser({
                email: data.email,
                password: data.password
            })
            // Mostra un messaggio di successo
            await createLog(`INFO: Accesso autorizzato ${data.email}`)
            swal.fire({
                title: 'Login avvenuto con successo!',
                text: 'Benvenuto, inizia a imparare.',
                icon: 'success',
                confirmButtonText: 'Inizia',
                confirmButtonColor: '#28a745'
            }).then(() => {
                navigateTo("/");
            });
        }
        catch (error) {
            // Mostra un messaggio di errore se le credenziali sono errate
            swal.fire({
                title: "Credenziali errate",
                icon: "error",
                confirmTextButton: "Riprovare",
              });
              swal.fire({
                title: "Qualcosa è andato storto...",
                text: 'Credenziali errate!',
                icon: "error",
                confirmButtonText: 'Riprova',
                confirmButtonColor: 'red'
            })
              // Registra un log dell'errore
              await createLog(`ERROR: Accesso negato ${data.email}`)
        }
    };
    // Render del form di login
    return (
        <>  
            <form onSubmit={handleSubmit(onSubmit)}>
                <h2 style={{fontFamily: 'monospace'}}>Login</h2>
                <br /><br />
                {/* Campo per la mail */}
                <div className="form-group">
                    <label htmlFor="email">E-mail</label>
                    <input
                        type="email"
                        className="form-control"
                        id="email"
                        placeholder="E-mail"
                        {...register("email", {
                            required: 'Questo campo è obbligatorio',
                            minLength: {
                                value: 1,
                                message: "Lunghezza minima: 1"
                            }
                        })}
                    />
                    <p>{errors.email?.message}</p>
                </div>
                {/* Campo per la password*/}
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        id="password"
                        placeholder="Password"
                        {...register("password", {
                            required: 'Questo campo è obbligatorio'
                        })}
                    />
                    <p>{errors.password?.message}</p>
                </div>
                {/* Pulsante per inviare il form */}
                <button type="submit" className="btn btn-success">Accedi</button>
            </form>
        </>
    );
}
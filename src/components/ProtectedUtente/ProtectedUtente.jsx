import { useSelector } from "react-redux";
import { Navigate} from "react-router-dom";

export function ProtectedUtente({children}) {

    const user = useSelector(state => state.user.value);

    console.log(user);

    if (user.isLogged) {
        console.log("NO");
        return <>
        {children}
        </>
    }
    else {
        console.log("Here");
        return <Navigate to={"/"} />
    }
}

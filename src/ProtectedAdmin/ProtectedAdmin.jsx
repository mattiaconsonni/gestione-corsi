import { useSelector } from "react-redux";
import { Navigate} from "react-router-dom";

export function ProtectedAdmin({children}) {

    const user = useSelector(state => state.user.value);

    console.log(user);

    if (user.isLogged && user.ruolo.includes("Admin")) {
        return <>
        {children}
        </>
    }
    else {
        return <Navigate to={"/"} />
    }
}

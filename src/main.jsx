import React from 'react'
import ReactDOM from 'react-dom/client'
import './main.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { StoreProvider } from './state/StoreProvider'
import { MainLayout } from './components/Layouts/MainLayout/MainLayout'
import { Homepage } from './components/Pages/Homepage/Homepage'
import { Registrazione } from './components/Pages/Registrazione/Registrazione'
import { Login } from './components/Pages/Login/Login'
import { Corsi } from './components/Pages/Corsi/Corsi'
import { ModificaCorso } from './components/Pages/ModificaCorso/ModificaCorso'
import { Profilo } from './components/Pages/Profilo/Profilo'
import { Utenti } from './components/Pages/Utenti/Utenti'
import { Logs } from './components/Pages/Logs/Logs'
import { ProtectedUtente } from './components/ProtectedUtente/ProtectedUtente'
import { ProtectedAdmin } from './ProtectedAdmin/ProtectedAdmin'
import { ProtectedModify } from './components/ProtectedModify/ProtectedModify'

const router = createBrowserRouter([
  {
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Homepage />
      },
      {
        path: "register",
        element: <Registrazione />
      },
      {
        path: "login",
        element: <Login />
      },
      {
        path: "corsi",
        element: (<ProtectedUtente>
                    <Corsi />
                  </ProtectedUtente>)
      },
      {
        path: "modifica",
        element: (<ProtectedModify>
                    <ModificaCorso />
                  </ProtectedModify>)
      },
      {
        path: "profilo",
        element: (<ProtectedUtente>
                    <Profilo />
                  </ProtectedUtente>)
      },
      {
        path: "utenti",
        element: (<ProtectedAdmin>
                    <Utenti />
                  </ProtectedAdmin>)
      },
      {
        path: "logs",
        element: (<ProtectedAdmin>
                    <Logs />
                  </ProtectedAdmin>)
      }
    ]
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <StoreProvider>
      <RouterProvider router={router}></RouterProvider>
    </StoreProvider>
  </React.StrictMode>,
)

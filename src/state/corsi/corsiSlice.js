import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    value: {
        corsiCorrenti: []
    }
}

const corsiSlice = createSlice({
    name: "corsi",
    initialState: initialState,
    reducers: {
        setCorsi: (state, action) => {
            state.value = action.payload;
        }
    },
});

export const corsiReducer = corsiSlice.reducer;
export const { setCorsi } = corsiSlice.actions;
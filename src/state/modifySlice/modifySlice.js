import { createSlice } from "@reduxjs/toolkit";

const initialState =  {
    value: {
        idCorso: -1
    }
}

const modifySlice = createSlice({
    name: "modify",
    initialState: initialState,
    reducers: {
        setModify: (state, action) => {
            state.value = action.payload;
        }
    }
}) 

export const modifyReducer = modifySlice.reducer;
export const {setModify} = modifySlice.actions;
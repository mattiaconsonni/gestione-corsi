import { configureStore } from "@reduxjs/toolkit";
import { userReducer } from "./user/userSlice";
import { corsiReducer } from "./corsi/corsiSlice";
import { modifyReducer } from "./modifySlice/modifySlice";

export const store = configureStore({
    reducer: {
        user: userReducer,
        corsi: corsiReducer,
        modify: modifyReducer
    },
});
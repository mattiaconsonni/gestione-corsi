import njdev from '../assets/njdev.jfif'
import cs from '../assets/cs.jpg';
import java from '../assets/java.jpg';
import emb from '../assets/emb.webp';
import ia from '../assets/ia.jpg'
import cripto from '../assets/cripto.jpg';
import { editCorso, deleteCorso, createCorso } from './firebase-service';


export const logo = njdev;

export const image = [
    {   
        titolo: "Cybersecurity",
        image: cs,
        descrizione: "Proteggi i tuoi dati e sistemi con corsi di sicurezza informatica (e la tua vita).",
        categoria: "cybersecurity"
    }, 
    {
        titolo: "Java",
        image: java,
        descrizione: "Padroneggia la programmazione spirituale Java con i nostri corsi completi.",
        categoria: "programmazione"
    },
    {
        titolo: "Sistemi Embedded",
        image: emb,
        descrizione: "Scopri i sistemi embeddati e le loro applicazioni sul campo.",
        categoria: "embedded"
    },
    {
        titolo: "Intelligenza Artificiale",
        image: ia,
        descrizione: "Esplora il mondo dell'intelligenza artificiale per sconfiggere lo IAShogun.",
        categoria: "ia"
    },
    {
        titolo: "Crittoanalisi",
        image: cripto,
        descrizione: "Comprendi i principi della crittoanalisi e della sicurezza per la tua patria.",
        categoria: "crittografia"
    },
]


export async function deletion(id) {
    await deleteCorso(id);
}

export async function editingCorso(id, campi) {
    await editCorso(id, campi);
}

export async function corsoGeneration(titolo, durata, categoria, descrizione) {
    await createCorso(titolo, parseInt(durata), categoria, descrizione);
}
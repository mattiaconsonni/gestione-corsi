import { addDoc, collection, deleteDoc, getDocs, doc, updateDoc} from "firebase/firestore";
import { db, auth } from "../config/firebase";
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
import Cookies from "js-cookie";

export function selectCollection(collectionName) {
    return collection(db, collectionName);
}

export async function getUsers() {
    const response = await getDocs(selectCollection("users"));

    const data = response.docs.map((doc) => ({id: doc.id, ...doc.data()}));
    return data;
}

export async function addUser(userData) {
    await addDoc(selectCollection("users"), userData);
}

export async function registerUser(userData) {
    const ud = {
        nome: userData.nome,
        cognome: userData.cognome,
        email: userData.email,
        role: ["Utente"],
        corsiSeguiti: []
    }
    const userCredentials = await createUserWithEmailAndPassword(
        auth,
        userData.email,
        userData.password
    )
    await addUser(ud);
    const user = userCredentials.user;
    return user;
}

export async function getUser(email) {
    const data = await getDocs(selectCollection("users"));
    const users = data.docs.map((doc) => ({id: doc.id, ...doc.data()}));
    return users.filter(element => element.email === email)[0];
}

export async function loginUser(formData) {
    const userCredentials = await signInWithEmailAndPassword(
      auth,
      formData.email,
      formData.password
    );
    const user = userCredentials.user;
    console.log(user);
    Cookies.set("token", user.accessToken, { expires: 7 });
    const userData = await getUser(user.email); 
    return userData;
  }

  export async function editUser(id, fields) {
    const userRef = doc(db, "users", id);
    await updateDoc(userRef, fields);
}

  export async function deleteUser(id) {
    const userRef = doc(db, 'users', id);
    await deleteDoc(userRef);
    const user = auth.currentUser;
    await user.delete();
  }

  // CORSI

  export async function getCorsi() {
    const response = await getDocs(selectCollection("corsi"));

    const data = response.docs.map((doc) => ({id: doc.id, ...doc.data()}));
    return data;
}


export async function createCorso(titolo, durata, categoria, descrizione) {
    const corso = {
        titolo: titolo,
        durata: durata,
        categoria: categoria,
        descrizione: descrizione
    };

    await addDoc(selectCollection("corsi"), corso);
}

export async function editCorso (id, fields) {
    const corsoRef = doc(db, "corsi", id);
    await updateDoc(corsoRef, fields);
}

export async function deleteCorso(id) {
    const corso = doc(db, "corsi", id);
    await deleteDoc(corso);
}


// Logs

export async function getLogs() {
    const response = await getDocs(selectCollection("logs"));

    const data = response.docs.map((doc) => ({id: doc.id, ...doc.data()}));
    return data;
}


export async function createLog(message) {
    const log = {
        date: new Date().toLocaleString(),
        message: message
    };

    await addDoc(selectCollection("logs"), log);
}

export async function deleteLog(id) {
    const log = doc(db, "logs", id);
    await deleteDoc(log);
}

    
